Source: leaflet-markercluster
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders:
 Andrew Harvey <andrew.harvey4@gmail.com>,
 Jonas Smedegaard <dr@jones.dk>,
Build-Depends:
 brotli,
 debhelper-compat (= 13),
 node-rollup-plugin-inject,
 node-rollup-plugin-json (>= 4.1),
 pandoc <!nodoc>,
 pigz,
 rename,
 rollup,
 sassc,
 uglifyjs (>= 3),
 terser,
Standards-Version: 4.6.2
Homepage: https://github.com/Leaflet/Leaflet.markercluster
Vcs-Browser: https://salsa.debian.org/js-team/leaflet-markercluster
Vcs-Git: https://salsa.debian.org/js-team/leaflet-markercluster.git
Rules-Requires-Root: no

Package: node-leaflet.markercluster
Architecture: all
Depends:
 libjs-leaflet.markercluster (= ${binary:Version}),
 node-leaflet,
 nodejs:any,
 ${misc:Depends},
Description: marker clustering functionality for Leaflet - Node.js library
 Leaflet.markercluster is a plugin for the Leaflet JavaScript library,
 providing beautiful animated marker clustering functionality.
 .
 Leaflet is a JavaScript library for mobile-friendly interactive maps.
 .
 This package provides Leaflet.markercluster library
 usable with Node.js - an event-based server-side JavaScript engine.

Package: libjs-leaflet.markercluster
Architecture: all
Depends:
 libjs-leaflet (>= 1),
 ${misc:Depends},
Recommends:
 javascript-common,
Breaks:
 libjs-leaflet-markercluster (<< 1.4.1~dfsg-4),
Replaces:
 libjs-leaflet-markercluster,
Multi-Arch: foreign
Description: marker clustering functionality for Leaflet - browser library
 Leaflet.markercluster is a plugin for the Leaflet JavaScript library,
 providing beautiful animated marker clustering functionality.
 .
 Leaflet is a JavaScript library for mobile-friendly interactive maps.
 .
 This package provides Leaflet.markercluster library
 directly usable in web browsers.

Package: libjs-leaflet-markercluster
Section: oldlibs
Architecture: all
Depends:
 libjs-leaflet.markercluster,
 ${misc:Depends},
Description: transitional package for libjs-leaflet.markercluster
 This is an empty transitional package
 to ease upgrading to libjs-leaflet.markercluster.
